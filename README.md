# vscodium

> Lucky configuration for vscodium

---

- [How to](#how-to)
  * [Before starting](#before-starting)
  * [General](#general)
  * [Snippets](#snippets)
  * [Shortcuts](#shortcuts)

---

# How to

## Before starting

> Extensions to install

```bash
- Markdown PDF
- Better Comments
```

## General

> General settings for vscodium

```bash
#Copy and Paste the files from the directory:
/general/
#to:
/home/user/.config/VSCodium/User/
```

## Styles

> Styles settings for markdown-pdf exportation

```bash
#Copy and Paste the files from the directory:
/styles/
#to a new directory called 'styles':
/home/user/.config/VSCodium/User/styles/
```

## Snippets

> Custom snippets

```bash
#Copy and Paste the files from the directory:
/snippets/
#to:
/home/user/.config/VSCodium/User/snippets/
```

## Shortcuts

> Shortcuts available after configuration

```bash
#insert a snippet:
CTRL+ALT+S

#export a markdown file:
CTRL+ALT+M
```